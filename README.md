<!--- *************************************************************************
      *Copyright 2024 CERTH
      *
      *Licensed under the Apache License, Version 2.0 (the "License"); you may not
      *use this file except in compliance with the License.  You may obtain a copy
      *of the License at
      *
      *  http://www.apache.org/licenses/LICENSE-2.0
      *
      *Unless required by applicable law or agreed to in writing, software
      *distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      *WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
      *License for the specific language governing permissions and limitations under
      *the License.
      ************************************************************************* --->

# Access Control

## Description
The access control module is responsible for user and access management. It is designed to ensure role-based access control (RBAC).

### Access Control within DATAMITE architecture
![architecture_AC](docs/figures/arch_extended_access_control.png)

### Access Control workflow
![workflow](docs/figures/AccessControlDiagram.png)

## Prerequisites
- Docker
- Docker Compose


## Installation
1. Clone the repository:
   ```
   git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-security/access-control.git
   ```

2. Navigate to the access-control folder:
   ```
   cd access-control
   ```

3. Start the services using Docker Compose:
   ```
   docker-compose up -d
   ```

4. The access control service will run on port 9090

## Database
PostgreSQL

## Developers
[Access Control Postman Collection](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-security/access-control/-/tree/main/postman_collection?ref_type=heads)


## Supported Roles - To be implemented for role based access control

1. **Data Owner (Business User):**
   - The individual responsible for datasets provided, authorizing which datasets will be shared, to whom, and for how long.

2. **Data Consumer (Business User):**
   - The user who will have access to data for business-related tasks and decision-making processes.

3. **Data Provider:**
   - Responsible for preparing, making available, integrating, and providing needed datasets/information to data consumers based on the directions of data owners.
   - Technical role that implements data owner directions.

4. **System Administrator:**
   - Responsible for the operation, management, and maintenance of the installed DATAMITE instance.

5. **Security/Privacy Responsible:**
   - Responsible for setting privacy and security policies.
   - Guides/consults users concerning related topics and oversees their policy implementation.

6. **Data Governance Responsible:**
   - Develops, consults, monitors, and enforces data governance policies and practices, including Data Quality.

## Roadmap and Functionalities:

- **M13-M14: Requirements Analysis and Design**
- **M15: Keycloak Configuration**
- **M16-M18: User Authentication & Identity and Access Management**
- **M18-M20: Modify Keycloak Theme based on DATAMITE's UI**
